---
layout: post
title:  "Pesquisas do Laser-CI estão entre as melhores do país"
date:   2019-04-12 10:00:00
image-path: "2019-04-12-pesquisa-laser/thumb.jpg"
abstract: "As áreas de Pesquisa Operacional e Robótica da UFPB, abrigadas no Laser, o Laboratório de Engenharia de Sistemas e Robótica do Centro de Informática (CI), estão incluídas no ranking das melhores do país, segundo aponta relatório do CSIndexbr (<a href=\"https://csindexbr.org/\" target=\"_blank\">https://csindexbr.org/</a>)."
---

![image]({{ site.baseurl }}/assets/news/2019-04-12-pesquisa-laser/thumb.jpg){: .w-50 .mx-auto .d-block}

As áreas de Pesquisa Operacional e Robótica da UFPB, abrigadas no Laser, o Laboratório de Engenharia de Sistemas e Robótica do Centro de Informática (CI), estão incluídas no ranking das melhores do país, segundo aponta relatório do CSIndexbr (<a href="https://csindexbr.org/" target="_blank">https://csindexbr.org/</a>).

Nesse levantamento, o CSIndexbr indexou trabalhos de pesquisa completos, publicados por professores brasileiros, em conferências e periódicos selecionados, desde 2014.

De acordo com o CSIndexbr, que fornece dados transparentes sobre a produção científica brasileira em Ciência da Computação, a produção científica em Pesquisa Operacional, desenvolvida na UFPB, coloca a instituição entre as cinco melhores do país, alcançando um score de 10.83 pontos, conforme mostra gráfico apresentado, nesta notícia. Em primeiro lugar está a Pontifícia Universidade Católica (PUC) do Rio de Janeiro, com um score de 16.75.

Na área de Robótica, a UFPB está colocada entre as oito mais bem avaliadas, ao lado da Universidade Federal do Ceará (UFC), a Universidade Federal do ABC (UFABC) e a Universidade Federal de São Paulo (Unifesp).

A Pesquisa Operacional é um ramo interdisciplinar da matemática aplicada que faz uso de modelos matemáticos, estatísticos e de algoritmos na ajuda à tomada de decisão. É usada sobretudo para analisar sistemas complexos do mundo real, tipicamente com o objetivo de melhorar ou otimizar a performance.

A Robótica é a ciência que estuda a elaboração, montagem e programação de robôs para execução de tarefas de forma automática.

Segundo Alisson Brito, da coordenação do Laser, esses dados revelam a qualidade e relevância da pesquisa desenvolvida no laboratório, que foi criado em 2013 com a proposta de desenvolver projetos e estudos em hardware e software, voltados às áreas de Robótica, Sistemas Embarcados e Inteligência Artificial.

Em 2018, o laboratório expandiu sua atuação, acrescentando as áreas de Logística, Transporte e Otimização e que vem se destacando por projetos de excelência também no segmento de Veículos Aéreos não Tripulados (Drones) e Robótica.

O Laser é um ponto de presença de empresas, que possuem laboratórios a ele integrados, visando o desenvolvimento de pesquisa aplicada, inovação e formação de recursos humanos. O Laser conta com a colaboração de mais de 70 jovens pesquisadores, entre doutores, estudantes de mestrado e graduação, todos em colaboração com grupos de pesquisa nacionais (como UFBA, UFCG e UFRN) e internacionais (de países como Portugal, Alemanha, Itália e República Tcheca), trazendo as atuais tendências tecnológicas mundiais nas suas áreas de pesquisa.


Fonte: Agência de Notícias da UFPB - Com Assessoria da Ascom do CI e CSIndexbr
